import React, { Component } from 'react';
import './App.css';

class ContainerComponent extends Component {
  constructor(props) {
    super(props);      
    this.state = {
      ListRegistroL: [
        {     
          img:     'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/desktop-max.svg', 
          D1: 'Crea tu cuenta en Coppel.com'
        },
        { 
          img: 'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/credit-card-max.svg', 
          D1: 'Agrega tu número de cliente'
        },
        { 
          img: 'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/mail-max.svg', 
          D1: 'Abre el correo de activación que recibiste'
        },
        { 
          img: 'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/checK-max.svg', 
          D1: 'Activa tu cuenta y ¡listo!' 
        }
      ],
      ListRegistroT: [
        { 
          img: 'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/user-max.svg', 
          D1: 'Visita tu tienda Coppel favorita y dirígete al área de Clientes '
        },
        { 
          img: 'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/credit-card-max.svg', 
          D1: 'Proporciona tu nombre o número de cliente y tu correo'
        },
        { 
          img: 'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/mail-max.svg', 
          D1: 'Abre el correo de activación que registraste'
        },
        { 
          img: 'https://cdn2.coppel.com/wcsstore/AuroraStorefrontAssetStore/emarketing/landings/abonoLinea/2020/marzo/checK-max.svg', 
          D1: 'Crea una contraseña y tu cuenta quedará activada' 
        }
      ]

    };
  }


  RenderL1(){
    return this.state.ListRegistroL.map(function(i,idx){
      return ( 
        <div key={'t1_'+idx} style={styles.t1}>
          <div style={{width:'15%'}}>
            <img src={i.img}></img>
          </div>
          <div style={{width:'85%'}}>
            {i.D1}
          </div>
        </div>
      )
    });

  }
  RenderL2(){
    return this.state.ListRegistroT.map(function(i,idx){
      return ( 
        <div key={'t1_'+idx} style={styles.t1}>
          <div style={{width:'15%'}}>
            <img src={i.img}></img>
          </div>
          <div style={{width:'85%'}}>
            {i.D1}
          </div>
        </div>
      )
    });
  }
  // ------------------------------
  // ------- render methods -------
  // ------------------------------
  render() {
    return (
      <div>
      <div class="container">
         <div style={{display: 'flex', alignItems: 'center', height: '90px' }} class="row">
         <div style={{ width: '50%' }} > 
         <img 
         style={{width: '200px', marginLeft: '50px'}} 
         src="https://eiacomercial.com/wp-content/uploads/2019/01/logo-coppel.jpg"
         >
         </img>
      </div>
      <div style={{ width: '50%', textAlign: 'right', marginRight: '50px'  }}>
      <a href="https://www.coppel.com/" >Volver a Coppel.com</a>
   </div>
   </div>
   </div>
   <div class="banner">        
      <div style={{padding: "43px 0px 45px 0px"}}>
      <div style={{ width: '100%', display: 'flex', alignItems: 'center'}}>
      <div style={{ width: '50%', marginLeft: '50px' }}> 
      <div style={{ width: '100%'}}><h1 style={{ width: '100%'}}>Abona a tu crédito Coppel donde quieras</h1>
   </div>
   <div>
      <h5>Por Coppel.com, con tu App y WhatsApp puedes abonar sin salir de casa</h5>
   </div>
   </div>
   <div style={{ width: '50%', textAlign: 'center'}}>
   <img src="https://www.coppel.com/wcsstore/AuroraStorefrontAssetStore/images/abonos/img-banner-img-desktop.png"></img>
   </div>
   </div>
   </div>
   </div>
   <div class="container" style={{marginTop: "30px", textAlign: 'center' }}>
   <div class="row">
      <div class="col">
         <iframe width="640" height="360" src="https://www.youtube.com/embed/4AlVfkWpUBc?enablejsapi=1"> </iframe>
      </div>
   </div>
   </div>
   <div class="container1" style={{marginTop: "30px"}}>
   <div class="row" style={{textAlign: "center"}} > 
   <h2>Sigue estos pasos para abonar en:</h2>
   </div>
   <div class="row">
      <div class="col">
         <div class="mycard">
            <div class="card-title">
               <p>Coppel.com</p>
            </div>
            <div class="card-content">
               <ol class="card-list">
                  <ul> Inicia sesión en tu cuenta Coppel, si todavía no la tienes, crea una cuenta en este momento</ul>
                  <ul> Si tienes crédito y aún no está ligado a tu cuenta de Coppel.com, usa tu número de cliente para ligar tu crédito aquí</ul>
                  <ul> Ve a la sección de Estado de cuenta y da clic en Abonar a crédito</ul>
                  <ul>
                     Escoge las cuentas que quieres pagar y la cantidad de tu abono
                     <p>Para pagar la cuenta de un amigo, amiga o familiar, ingresa el número de cliente al que quieres abonar; lo encuentras al frente de la tarjeta o el estado de cuenta</p>
                  </ul>
                  <ul>  Paga con cualquier tarjeta bancaria, menos con crédito BanCoppel </ul>
               </ol>
               <div>
               </div>
               <div>
                  <a>Crear una cuenta</a>
               </div>
            </div>
         </div>
      </div>
      <div class="col">
         <div class="mycard">
            <div class="card-title">
               <p>App Coppel</p>
            </div>
            <div class="card-content">
               <ol class="card-list">
                  <ul> Abre la App Coppel en tu celular e inicia sesión, si todavía no tienes cuenta, crea una desde tu aplicación o en Coppel.com</ul>
                  <ul> Entra a la sección Abonos o en Estado de cuenta si ya eres cliente</ul>
                  <ul>
                     Escoge las cuentas que quieres pagar y la cantidad de tu abono
                     <p>Para pagar la cuenta de un amigo, amiga o familiar, ingresa el número de cliente al que quieres abonar; lo encuentras al frente de la tarjeta o en estado de cuenta</p>
                  </ul>
                  <ul> Paga con cualquier tarjeta bancaria, menos con crédito BanCoppel</ul>
               </ol>
               <spam>Descarga desde:</spam>
               <img src=""></img>
               <img src=""></img>
            </div>
         </div>
      </div>
      <div class="col">
         <div class="mycard">
            <div class="card-title">
               <p>WhatsApp</p>
            </div>
            <div class="card-content">
               <ol class="card-list">
                  <ul> Registra tu número celular en el servicio de WhatsApp, necesitarás tu número de cliente</ul>
                  <ul>  Asegúrate de guardar el número y que la cuenta tenga la siguiente insignia verde Icono de insignia verde de WhatsApp al lado del nombre del contacto, lo cual confirma que es una cuenta verificada por WhatsApp</ul>
                  <ul> Abre una conversación y dinos "Hola" </ul>
                  <ul> Sigue las instrucciones y abona a tus cuentas con una tarjeta de débito BanCoppel </ul>
               </ol>
            </div>
         </div>
      </div>
   </div>
   </div>
   <div>Beneficios</div>
   <div>para poder abonar</div>
   <div>pie</div>
   <div style={styles.C1} >
      <div style={styles.C2}>
         <div style={styles.ti1}> Regístrate en línea</div>
         <hr>
         </hr>
         { this.RenderL1()}
      </div>
      <div style={styles.C2}>
         <div style={styles.ti1}> Regístrate en tienda</div>
         <hr>
         </hr>
         { this.RenderL2()}
      </div>
   </div>
   </div>    );
  }
}


// estilos
const styles = {
  container: {
    position: 'absoluete',
    height: 'calc(100% - 50px)',
    background: '#f4f4f4'
  },
  C1:{
    border: '1px solid black',
    width: '100%',
    display: 'flex',
    alignItems: 'center'
  },

  C2:{
    border: '1px solid #f9fafb',
    borderRadius: '10px',
    width: '40%',
    margin: '5%',
    padding: '10px'
  },
t1:{
  width: '100%',
  display: 'flex',
  alignItems: 'center'
},
ti1:{
  widt: '100%',
  padding: '10px',
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#1f1f1f',
  boxSizing: 'border-box'
},
  img1: {
    with: '100px',
    height: '100px'
  }
};


// exportar componente
export default ContainerComponent;
